<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Chris</firstname>">
  <!ENTITY dhsurname   "<surname>Wesley</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>September 29, 2002</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>chris@cwwesley.net</email>">
  <!ENTITY dhusername  "Chris Wesley">
  <!ENTITY dhucpackage "<refentrytitle>WMDRAWER</refentrytitle>">
  <!ENTITY dhpackage   "wmdrawer">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2002</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>

  <refmeta>
    &dhucpackage;
    &dhsection;
  </refmeta>

  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>Applicazione per Window Maker che fornisce un "cassetto"</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option>-c <replaceable>config_file</replaceable></option></arg>
      <arg><option>-w</option></arg>
      <arg><option>-h</option></arg>
      <arg><option>-v</option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1>
    <title>DESCRIZIONE</title>
    <para><command>&dhpackage;</command> � un'applicazione per WindowMaker
    che fornisce un'icona-cassetto (men� rientrante) per lanciare
    comodamente le applicazioni usate pi� frequentemente.
    </para>
    <para>Il cassetto � retraibile ed animato.</para>
    <para>Nel cassetto possono essere definite pi� colonne di pulsanti.</para>
    <para>Il file di configurazione � automaticamente riletto ogni volta si effettua una modifica.</para>
    <para>Si possono eseguire istanze multiple del programma 
    contemporaneamente.</para>
    <para>Caratteristiche configurabili: apertura e chiusura manuale/automatica 
    del cassetto, forma del cursore all'interno del cassetto, direzione di 
    apertura e velocit� di animazione del cassetto, icona del cassetto 
    ed immagine di sfondo delle icone.</para>
  </refsect1>

  <refsect1>
    <title>OPZIONI</title>

    <variablelist>
      <varlistentry>
        <term><option>-c &lt;config_file&gt;</option>
        </term>
        <listitem>
          <para>Usa  config_file come file di configurazione al posto del default (<filename>~/.wmdrawerrc</filename>)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-w</option>
        </term>
        <listitem>
          <para>Esegui in una finestra. Utile per gli utenti di AfterStep, Fvwm2, Sawfish e KDE.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-h</option>
        </term>
        <listitem>
          <para>Mostra l'elenco delle opzioni.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-v</option>
        </term>
        <listitem>
          <para>Mostra la versione del programma.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>FILES</title>
    <para><filename>~/.wmdrawerrc</filename> � il file di configurazione
    di default per ogni utente. Ecco un esempio completamente commentato:
    </para>
<para><programlisting>
# Esempio di file di configurazione per wmdrawer
[general]
# Icona dell'applicazione [opzionale]
dock_icon       panel-drawer.png

# Immagine usata per lo sfondo delle icone [opzionale]
icons_bg        defaultTile.xpm

# Direzione di apertura del cassetto
# Dall'alto verso il basso = 0
# Da destra verso sinistra = 1
# Dal basso verso l'alto = 2
# Da sinistra verso destra = 3
direction       2

# Velocit� di animazione del cassetto: 1, 2, 3, 4 [opzionale, default 1]
animation_speed 1

# Modo del cursore nel cassetto: 0, 1, 2 [opzionale, default 2]
cursor          2

# Apertura automatica del cassetto: 0, 1 [opzionale, default 0]
show_on_hover   0

# Chiusura automatica del cassetto: 0, 1 [opzionale, default 0]
hide_on_out     0

# Lancia l'applicazione in una finestra: 0, 1 [opzionale, default 0]
# (Utile se si usa Fvwm2, AfterStep, Sawmill, KDE)
windowed_mode   0

# Cartelle delle icone
[images_paths]
/usr/share/pixmaps
/usr/local/share/pixmaps/

# Una prima colonna con 5 pulsanti
# Ogni pulsante deve essere definito secondo la seguente sintassi:
# immagine&lt;SPAZIO e/o TABS&gt;comando
[column]
gnome-term.png	     xterm
galeon.xpm           galeon -s
sylpheed-vanilla.xpm sylpheed
xchat.png	     xchat
gnome-multimedia.png mplayer

# Una seconda colonna con 5 pulsanti
[column]
gnome-audio2.png xmms
gnome-gimp.png   gimp
gqview.png       gqview
gnome-calc2.png  xcalc
gftp.png         gftp
</programlisting></para>
    <para>
    Una copia di questo esempio la si pu� trovare in <filename>/usr/share/doc/wmdrawer/wmdrawerrc.example</filename>.
    </para>
  </refsect1>

  <refsect1>
    <title>VEDERE ANCHE</title>
    <para>wmaker (1)</para>
  </refsect1>

  <refsect1>
    <title>AUTORI</title>
    <para>Questo programma � scritto e mantenuto da Val�ry Febvre
      (<email>vfebvre@vfebvre.lautre.net</email>).</para>
    <para>Questa pagina di manuale � stata scritta da &dhusername; (&dhemail;)
      per il sistema &debian; (ma pu� essere usata da altri).
      � permessa la copia, la distribuzione e/o la modifica di questo
      documento sotto i termini della <acronym>GNU</acronym> Free Documentation
      License, Versione 1.1 o ogni versione successiva pubblicata dalla
      Free Software Foundation; senza Invariant Sections,  Front-Cover
      Texts e Back-Cover Texts.</para>
    <para>La traduzione italiana � stata effettuata da Fabio Stumbo
      (<email>fabio.stumbo@unife.it</email>).</para>
  </refsect1>

  <refsect1>
    <title>ALTRE INFORMAZIONI</title>
    <para>La webpage di  wmdrawer � http://people.easter-eggs.org/~valos/wmdrawer/wmdrawer.html.</para>
    <para>Sul sito si possono trovare informazioni varie su wmdrawer e scaricare l'ultima versione.</para>
  </refsect1>

</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
