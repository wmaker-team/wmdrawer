/* wmDrawer (c) 2002-2004 Valery Febvre <vfebvre@vfebvre.lautre.net>
 *
 * wmDrawer is a dock application (dockapp) which provides a
 * drawer (button bar) to launch applications from.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <sys/types.h>
#include <regex.h>

#include "types_defs.h"
#include "utils.h"

void dbg_msg (int level, const char *format, ...) {
  if (level <= DBG_LEVEL) {
    va_list va;
    
    assert (format != NULL);
    va_start (va, format);
    vprintf (format, va);
    va_end (va);
  }
}

void *xcalloc (size_t nmemb, size_t size) {
  register void *value = calloc (nmemb, size);

  if (value == 0) {
    printf ("%s error: virtual memory exhausted\n", PACKAGE);
  }
  return value;
}

void *xrealloc (void *ptr, size_t size) {
  register void *value = realloc (ptr, size);

  if (value == 0) {
    printf ("%s error: virtual memory exhausted\n", PACKAGE);
  }
  return value;
}

int match_regex (char *string, const char *regex) {
  regex_t preg;
  int res;
  
  /* compile regex */
  res = regcomp (&preg, regex, REG_EXTENDED | REG_NOSUB);
  if (res != 0) {
    printf ("%s error: could not compile regex\n", PACKAGE);
    exit (EXIT_FAILURE);
  }
  /* execute regex */
  res = regexec (&preg, string, 0, NULL, 0);
  /* free compiled regex */
  regfree (&preg);
  if (res == 0) {
    return 1;
  }
  else return 0;
}
