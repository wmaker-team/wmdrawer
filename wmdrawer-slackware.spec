wmdrawer: wmdrawer (WindowMaker dockapp)
wmdrawer:
wmdrawer: wmdrawer is a dock application (dockapp) which provides a drawer
wmdrawer: (retractable button bar) to launch applications.
wmdrawer:
wmdrawer: wmdrawer requires gdk-pixbuf, gtk+ and glib.
wmdrawer:
wmdrawer: wmDrawer is Copyright (C) 2002-2004 by Valery Febvre, FRANCE
wmdrawer:   <vfebvre@vfebvre.lautre.net>
wmdrawer: Homepage : http://people.easter-eggs.org/~valos/wmdrawer/
wmdrawer: Slackware packaging provided by Ed Goforth <e.goforth@computer.org>
